package com.TomaszNowalski.ProjektTechnologieJE;

import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class DateBuilder {
    public Date BuildDateByString(String dateInString) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.parse(dateInString);
    }
}
