package com.TomaszNowalski.ProjektTechnologieJE.basicClass;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
public class Firma {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NonNull
    //@FirmaName
    private String nazwaFirmy;

    @OneToOne
    private Osoba prezesFirmy;


    @Override
    public String toString() {
        if (prezesFirmy != null)
            return "Firma{" +
                    "nazwaFirmy='" + nazwaFirmy + '\'' +
                    ", prezesFirmy= id:" + prezesFirmy.getId() +
                    '}';
        else
            return "Firma{" +
                    "nazwaFirmy='" + nazwaFirmy + '\'' +
                    ", prezesFirmy=brak" +
                    '}';
    }
}
