package com.TomaszNowalski.ProjektTechnologieJE.basicClass;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@NoArgsConstructor
public class FirmaDto {

    private int ID;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+", message = "nazwa zaczynac sie z wielkiej litery, skladac sie wylacznie z liter i miec wiecej niz jeden znak")
    private String name;

    @NotNull
    private int presidentID;


}
