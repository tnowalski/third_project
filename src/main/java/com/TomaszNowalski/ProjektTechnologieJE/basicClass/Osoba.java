package com.TomaszNowalski.ProjektTechnologieJE.basicClass;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
public class Osoba {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String imie;
    private String nazwisko;
    private String email;

    @OneToOne
    private Firma firma;

    private Date dataZatrudnienia;
}
