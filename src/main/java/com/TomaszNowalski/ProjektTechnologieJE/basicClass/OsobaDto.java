package com.TomaszNowalski.ProjektTechnologieJE.basicClass;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OsobaDto {

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+", message = "Imie musi zaczynac sie z wielkiej litery, skladac sie wylacznie z liter i miec wiecej niz jeden znak")
    private String imie;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+", message = "Nazwisko musi zaczynac sie z wielkiej litery, skladac sie wylacznie z liter i miec wiecej niz jeden znak")
    private String nazwisko;

    @NotNull
    @Pattern(regexp = ".*@[A-Za-z0-9]+.[a-z]+", message = "Nieprawidlowy adres email")
    private String email;

    @NotNull
    @Pattern(regexp = ".{8,}", message = "Zbyt krotkie haslo, conajmniej 8 znakow")
    @Pattern(regexp = ".*[a-z].*", message = "Conajmniej jedna mala litera")
    @Pattern(regexp = ".*[A-Z].*", message = "Conajmniej jedna wielka litera")
    @Pattern(regexp = ".*[0-9].*", message = "Conajmniej jedna cyfra")
    @Pattern(regexp = ".*[!#$%&'()*+,-./:;<=>?@\\[\\]^_`{|}~].*", message = "Conajmniej jeden znak specjalny")
    private String password;

}
