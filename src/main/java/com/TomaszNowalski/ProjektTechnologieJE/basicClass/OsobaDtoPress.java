package com.TomaszNowalski.ProjektTechnologieJE.basicClass;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OsobaDtoPress {

    @NotNull
    private Osoba osoba;

    @NotNull
    private boolean Press;

    private List<String> firmaNames;
}
