package com.TomaszNowalski.ProjektTechnologieJE.controlers;

import com.TomaszNowalski.ProjektTechnologieJE.basicClass.Firma;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.FirmaDtoPress;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.Osoba;
import com.TomaszNowalski.ProjektTechnologieJE.repositorys.SecurityRepository;
import com.TomaszNowalski.ProjektTechnologieJE.services.FirmaService;
import com.TomaszNowalski.ProjektTechnologieJE.services.OsobaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
public class FirmaController {

    @Autowired
    OsobaServices osobaServices;

    @Autowired
    FirmaService firmaService;

    @Autowired
    SecurityRepository securityRepository;

    @RequestMapping("/firmy")
    public String getFirmy(Model model, Principal principal) {
        List<Firma> firmySet;
        if(securityRepository.getRoleByName(principal.getName()).equals("ADMIN"))
            firmySet = firmaService.GetAllFirma();
        else
            firmySet = firmaService.GetAllFirma(principal.getName());
        model.addAttribute("firmy", firmySet);
        return "firmy";
    }

    @RequestMapping("/firmaAdd")
    public String addFirma(Model model,  Principal principal) {
        List<Osoba> osobyList;
        if(securityRepository.getRoleByName(principal.getName()).equals("ADMIN"))
            osobyList = osobaServices.GetAllOsoba();
        else
            osobyList = osobaServices.GetOsobaOnList(principal.getName());

        FirmaDtoPress firmaDtoPress = new FirmaDtoPress();
        firmaDtoPress.setFirmaNames(osobyList);
        model.addAttribute("firma", firmaDtoPress);
        return "firmaAdd";
    }

    @RequestMapping(value = "/firmaAddMethod", method = RequestMethod.POST)
    public String saveFirma(@Valid @ModelAttribute(value="firma") FirmaDtoPress firmaDBOPrees, BindingResult bindingResult,   Principal principal) {
        if(bindingResult.hasErrors()){
            List<Osoba> osobyList;
            if(securityRepository.getRoleByName(principal.getName()).equals("ADMIN"))
                osobyList = osobaServices.GetAllOsoba();
            else
                osobyList = osobaServices.GetOsobaOnList(principal.getName());

            firmaDBOPrees.setFirmaNames(osobyList);
            return "/firmaAdd";
        }
        else{
            firmaService.SaveFirma(firmaDBOPrees, principal.getName());
            return "redirect:/firmy";
        }
    }


    @RequestMapping(value = "/firmaRemove/{id}")
    public String removeFirma(@PathVariable("id") String name) {
        firmaService.RemoveFirma(name);
        return "redirect:/firmy";
    }

    @RequestMapping("/firmaAddOsoba/{id}")
    public String firmaAddOsosba(@PathVariable("id") String name, Model model) {
        List<Osoba> osobyList = osobaServices.GetOsobaWithoutWork();
        model.addAttribute("osoby", osobyList);
        model.addAttribute("o", new Osoba());
        model.addAttribute("name", name);
        return "firmaAddOsoba";
    }

    @RequestMapping(value = "/firmaAddOsosbaMethod/{id}", method = RequestMethod.POST)
    public String firmaAddOsosbaMethod(@PathVariable("id") String name, Osoba o) {
        osobaServices.AddFirmaToOsoba(name, o.getId());
        return "redirect:/firmy";
    }

}

