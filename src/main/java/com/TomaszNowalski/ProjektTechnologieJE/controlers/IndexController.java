package com.TomaszNowalski.ProjektTechnologieJE.controlers;

import com.TomaszNowalski.ProjektTechnologieJE.basicClass.Firma;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.Osoba;
import com.TomaszNowalski.ProjektTechnologieJE.repositorys.SecurityRepository;
import com.TomaszNowalski.ProjektTechnologieJE.services.FirmaService;
import com.TomaszNowalski.ProjektTechnologieJE.services.OsobaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

@Controller
public class IndexController {

    @Autowired
    OsobaServices osobaServices;

    @Autowired
    FirmaService firmaService;

    @Autowired
    SecurityRepository securityRepository;

    @RequestMapping(value ="/")
    public String getOsoby(Model model, Principal principal) {
        if(securityRepository.getRoleByName(principal.getName()).equals("ADMIN")){
            return "admin";
        }

        else{
            Osoba osoba = osobaServices.GetOsoba(principal.getName());
            List<Firma> firmySet = firmaService.GetAllFirma(principal.getName());
            model.addAttribute("firmy", firmySet);
            model.addAttribute("osoba", osoba);
            return "index";
        }
    }

    @RequestMapping(value = "/osobaRemoveAndLogout/{id}")
    public String removeOsoba(@PathVariable("id") Integer id) {
        osobaServices.RemoveOsoba(id);
        return "redirect:/logout";
    }

}
