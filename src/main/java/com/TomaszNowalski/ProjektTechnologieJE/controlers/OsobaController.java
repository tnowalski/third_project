package com.TomaszNowalski.ProjektTechnologieJE.controlers;

import com.TomaszNowalski.ProjektTechnologieJE.basicClass.Osoba;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.OsobaDto;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.OsobaDtoPress;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.User;
import com.TomaszNowalski.ProjektTechnologieJE.repositorys.SecurityRepository;
import com.TomaszNowalski.ProjektTechnologieJE.services.FirmaService;
import com.TomaszNowalski.ProjektTechnologieJE.services.OsobaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
public class OsobaController {


    @Autowired
    OsobaServices osobaServices;

    @Autowired
    FirmaService firmaService;

    @Autowired
    SecurityRepository securityRepository;

    @PersistenceContext
    EntityManager em;

    @RequestMapping(value ="/osoby", method = RequestMethod.GET)
    public String getOsoby(Model model, Principal principal) {
        List<OsobaDtoPress> osobySet;
        if(securityRepository.getRoleByName(principal.getName()).equals("ADMIN"))
            osobySet = osobaServices.GetAllOsobaDtoPress();
        else
            osobySet = osobaServices.GetAllOsobaDtoPress(principal.getName());
        model.addAttribute("osoby", osobySet);
        for(OsobaDtoPress o : osobySet){
        }
        return "osoby";
    }

    @RequestMapping("/register")
    public String register(Model model) {
        model.addAttribute("osoba", new OsobaDto());
        return "register";
    }

    @RequestMapping(value = "/registerMethod", method = RequestMethod.POST)
    public String registerMethod(@Valid @ModelAttribute(value="osoba") OsobaDto osobaDto, BindingResult bindingResult, Principal principal) {
        if(bindingResult.hasErrors()){
            return "register";
        }
        else{
            osobaServices.SaveOsoba(osobaDto, principal.getName());
            User actuallyUser = em.createQuery("select u from User u where u.name like '"+ principal.getName() +"'", User.class).getSingleResult();
            if(!actuallyUser.getRole().equals("ADMIN"))
                return "redirect:login";
            else
                return "redirect:/osoby";
        }
    }

    @RequestMapping("/osobaEdit")
    public String edit(Model model, Principal principal) {
        Osoba osoba = osobaServices.GetOsoba(principal.getName());
        OsobaDto osobaDto = new OsobaDto();
        osobaDto.setImie(osoba.getImie());
        osobaDto.setNazwisko(osoba.getNazwisko());
        osobaDto.setEmail(osoba.getEmail());
        model.addAttribute("osoba", osobaDto);
        model.addAttribute("dane", osoba);
        return "osobaEdit";
    }

    @RequestMapping(value = "/osobaEditMethod", method = RequestMethod.POST)
    public String editMethod(@Valid @ModelAttribute(value="osoba") OsobaDto osobaDto, BindingResult bindingResult, Principal principal) {
        if(bindingResult.hasErrors()){
            return "osobaEdit";
        }
        else{
            osobaServices.EditOsoba(osobaDto, principal.getName());
                return "redirect:/logout";
        }
    }

    @RequestMapping(value = "/osobaRemove/{id}")
    public String removeOsoba(@PathVariable("id") Integer id) {
        osobaServices.RemoveOsoba(id);
        return "redirect:/osoby";
    }

    @RequestMapping(value = "osobaFiltr/osobaRemove/{id}/{name}")
    public String removeOsoba2(@PathVariable("id") Integer id, @PathVariable("name") String name) {
        osobaServices.RemoveOsobaWithFirma(id);
        return "redirect:/osobaFiltr/" + name;
    }

    @RequestMapping("/osobaFiltr/{name}")
    public String osobaFilter(@PathVariable("name") String name, Model model, Principal principal) {
        List<OsobaDtoPress> osoby = firmaService.GetWorkerBtFirmaName(name, principal.getName());
        model.addAttribute("osoby", osoby);
        return "/osobaFiltr";
    }

}
