package com.TomaszNowalski.ProjektTechnologieJE.controlers;

import com.TomaszNowalski.ProjektTechnologieJE.services.SecurityServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SecurityController {

   @Autowired
   SecurityServices securityServices;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(ModelAndView mav){
    mav.setViewName("login");
    return mav;
    }

  @RequestMapping(value = "/account_active/{id}")
  public String accountActive(@PathVariable("id") Integer id){
   securityServices.accountActive(id);
   return "redirect:/";
  }

    @RequestMapping(value = "/webjars/bootstrap/3.3.7-1/js/bootstrap.min.js")
    public String login(){
        return "redirect:/";
    }

    @RequestMapping(value = "/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css")
    public String login2(){
        return "redirect:/";
    }



}
