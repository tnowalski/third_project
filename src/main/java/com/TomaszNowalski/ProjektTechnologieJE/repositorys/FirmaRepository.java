package com.TomaszNowalski.ProjektTechnologieJE.repositorys;

import com.TomaszNowalski.ProjektTechnologieJE.Mailer;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.Firma;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.Osoba;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.OsobaDtoPress;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.User;
import com.TomaszNowalski.ProjektTechnologieJE.services.OsobaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class FirmaRepository {

    @Autowired
    OsobaRepository osobaRepository;

    @Autowired
    OsobaServices osobaServices;

    @Autowired
    Mailer mailer;

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void AddFirma(String name, int presidentID, String userName) {
        Firma firma = new Firma(name);
        firma.setPrezesFirmy(osobaRepository.GetOsoba(presidentID));
        em.persist(firma);
        Osoba osoba = em.find(Osoba.class, presidentID);
        if (osoba.getFirma() == null) {
            osoba.setFirma(firma);
            osoba.setDataZatrudnienia(new Date());
            em.merge(osoba);
        }

        User user = em.createQuery("select u from User u where u.name like '"+ userName +"'", User.class).getSingleResult();
        if(!user.getRole().equals("ADMIN"))
            mailer.sendMail(firma.getPrezesFirmy().getEmail(), "Stworzono firmę", "Pomyślnie utworzono firmę " + firma.getNazwaFirmy() + "w aplikacji JE");
    }

    @Transactional
    public void EditFirma(int firmaID, String name, int presidentID) {
        Firma firma = em.find(Firma.class, firmaID);
        firma.setNazwaFirmy(name);
        firma.setPrezesFirmy(em.find(Osoba.class, presidentID));
        em.merge(firma);
    }

    @Transactional
    public void RemoveFirma(String name) {
        Firma firma = GetFirma(name);
        for (Osoba osoba : em.createQuery("select o from Osoba o where o.firma.id like " + firma.getId(), Osoba.class).getResultList()) {
            osoba.setFirma(null);
            osoba.setDataZatrudnienia(null);
            em.merge(osoba);
        }
        em.remove(em.find(Firma.class, firma.getId()));
    }

    public Firma GetFirma(String name) {
        Firma firma = em.createQuery("select f from Firma f where f.nazwaFirmy like '" + name + "'", Firma.class).getSingleResult();
        return firma;
    }

    public List<Firma> GetAllFirma() {
        return em.createQuery("select f from Firma f", Firma.class).getResultList();
    }

    public List<Firma> GetAllFirma(String name) {
        User user = em.createQuery("select u from User u where u.name like '"+ name +"'", User.class).getSingleResult();
        return em.createQuery("select f from Firma f where f.prezesFirmy.id like "+ user.getOsoba().getId(), Firma.class).getResultList();
    }

    public List<Firma> GetAllOsobaFirma(int id) {
        return em.createQuery("select f from Firma f where f.prezesFirmy.id like "+ id, Firma.class).getResultList();
    }

    public List<OsobaDtoPress> GetOsobaWithThisFirma(String firmaName, String userName) {
        List<Osoba> osobaList = new ArrayList<>();
        List<OsobaDtoPress> osobaDtoPressList = new ArrayList<>();

        osobaList.addAll(em.createQuery("select o from Osoba o where o.firma.nazwaFirmy like '" + firmaName + "'", Osoba.class).getResultList());
        User user = em.createQuery("select u from User u where u.name like '"+ userName +"'", User.class).getSingleResult();

        if(user.getRole().equals("ADMIN")){
           Osoba preses = osobaRepository.GetPreses(firmaName);
           if(!osobaList.contains(preses))
               osobaList.add(preses);
       }
       else{

           osobaList.remove(em.find(Osoba.class, user.getOsoba().getId()));
       }

        for(Osoba osoba : osobaList)
            osobaDtoPressList.add(new OsobaDtoPress(osoba, osobaRepository.IsPress(osoba.getId()), new ArrayList<>()));
        return  osobaDtoPressList;
    }

}
