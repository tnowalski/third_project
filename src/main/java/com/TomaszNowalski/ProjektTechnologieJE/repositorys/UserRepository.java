package com.TomaszNowalski.ProjektTechnologieJE.repositorys;

import com.TomaszNowalski.ProjektTechnologieJE.Mailer;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.Osoba;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Component
public class UserRepository {


    @PersistenceContext
    EntityManager em;

    @Autowired
    Mailer mailer;

    @Transactional
    public void AddUser(Osoba osoba, String password, String userName) {
        User user = new User();
        user.setName(osoba.getEmail());
        user.setPassword(password);
        user.setOsoba(osoba);
        user.setRole("USER");

        User actuallyUser = em.createQuery("select u from User u where u.name like '"+ userName +"'", User.class).getSingleResult();
        if(!actuallyUser.getRole().equals("ADMIN"))
        {
            user.setEnable(false);
            mailer.sendMail(user.getOsoba().getEmail(), "Potwierdzenie Rejestracji ", "Zarejestrowano użytkownika " + user.getOsoba().getImie() + " " + user.getOsoba().getNazwisko() + "\nLink aktywacyjny: http://localhost:8080/account_active/"+ user.getId());
        }
        else {
            user.setEnable(true);
        }
        em.persist(user);

  }
}
