package com.TomaszNowalski.ProjektTechnologieJE.services;

import com.TomaszNowalski.ProjektTechnologieJE.basicClass.Firma;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.FirmaDto;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.FirmaDtoPress;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.OsobaDtoPress;
import com.TomaszNowalski.ProjektTechnologieJE.repositorys.FirmaRepository;
import com.TomaszNowalski.ProjektTechnologieJE.repositorys.OsobaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FirmaService {

    @Autowired
    FirmaRepository firmaRepository;

    @Autowired
    OsobaRepository osobaRepository;

    public List<Firma> GetAllFirma() {
        return firmaRepository.GetAllFirma();
    }
    public List<Firma> GetAllFirma(String name) {
        return firmaRepository.GetAllFirma(name);
    }

    public void RemoveFirma(String name) {
        firmaRepository.RemoveFirma(name);
    }

    public void SaveFirma(FirmaDtoPress firmaDBO, String userName) {
            firmaRepository.AddFirma(firmaDBO.getName(), firmaDBO.getPresidentID(), userName);
    }

    public void EditFirma(FirmaDto firmaDBO) {
            firmaRepository.EditFirma(firmaDBO.getID(), firmaDBO.getName(), firmaDBO.getPresidentID());
    }

    public List<OsobaDtoPress> GetWorkerBtFirmaName(String name, String userName) {
        return firmaRepository.GetOsobaWithThisFirma(name, userName);
    }


}
