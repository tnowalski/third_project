package com.TomaszNowalski.ProjektTechnologieJE.services;

import com.TomaszNowalski.ProjektTechnologieJE.DateBuilder;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.Osoba;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.OsobaDto;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.OsobaDtoPress;
import com.TomaszNowalski.ProjektTechnologieJE.repositorys.FirmaRepository;
import com.TomaszNowalski.ProjektTechnologieJE.repositorys.OsobaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OsobaServices {

    @Autowired
    FirmaRepository firmaRepository;

    @Autowired
    OsobaRepository osobaRepository;

    @Autowired
    DateBuilder dateBuilder;

    public List<OsobaDtoPress> GetAllOsobaDtoPress(String id) {
    return osobaRepository.GetAllOsobaDtoPress(id);
    }

    public List<OsobaDtoPress> GetAllOsobaDtoPress() {
        return osobaRepository.GetAllOsobaDtoPress();
    }

    public List<Osoba> GetAllOsoba(String id) {
        return osobaRepository.GetAllOsoba(id);
    }

    public List<Osoba> GetAllOsoba() {
        return osobaRepository.GetAllOsoba();
    }

    public void SaveOsoba(OsobaDto osobaDto, String userName) {
            osobaRepository.AddOsoba(osobaDto.getImie(), osobaDto.getNazwisko(), osobaDto.getEmail(), osobaDto.getPassword(), userName);
    }
    public void EditOsoba(OsobaDto osobaDto, String userName){
        osobaRepository.EditOsoba(osobaDto.getImie(),osobaDto.getNazwisko(),osobaDto.getEmail(),osobaDto.getPassword(),userName);
    }

    public void RemoveOsoba(int id) {
        osobaRepository.RemoveOsoba(id);
    }

    public void RemoveOsobaWithFirma(int id) {
        osobaRepository.RemoveOsobaWithFirma(id);
    }

 /* public Osoba GetOsoba(int id) {
            return osobaRepository.GetOsoba(id);
    }*/

    public Osoba GetOsoba(String name) {
        return osobaRepository.GetOsoba(name);
    }

    public List<Osoba> GetOsobaOnList(String name) {
        return osobaRepository.GetOsobaOnList(name);
    }

    public List<Osoba> GetOsobaWithoutWork() {
        return osobaRepository.GetOsobaWithoutWork();
    }

    public void AddFirmaToOsoba(String firmaName, int idOsoba) {
         osobaRepository.AddFirmaToOsoba(firmaName, idOsoba);
    }

}
