package com.TomaszNowalski.ProjektTechnologieJE.services;

import com.TomaszNowalski.ProjektTechnologieJE.repositorys.SecurityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SecurityServices {

    @Autowired
    SecurityRepository securityRepository;

    public void accountActive(int id){
        securityRepository.accountActive(id);
    }
}
